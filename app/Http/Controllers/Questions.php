<?php

namespace App\Http\Controllers;

use App\question;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class Questions extends Controller
{
    public function getIndex(){
        return view('questions.index')
                ->with('title', 'Make It Sanppy Q&A -Home');
    }

    public function postQuestion(){
        $input =Input::all();

        $rules= array(
            'question'=>'required|min:4|max:255',
            'solved'=>'in:0,1'
        );

        $v=Validator::make($input, $rules);

        if($v->fails()){
            return Redirect::to('home/index')->withErrors($v);
        }else{
            $question=new Question();

            $question->question = $input['question'];
            $question->question_id=Auth::user()->id;
            if($question->save());

            return Redirect::to('home/index')->with(
                'msg', 'Successfully Posted'
            );


        }
    }
}
