<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getNew()
    {
        return view('users.new')->with('title', 'Make It Snappy Q&A ->Register');
    }

    public function postNew(){
        $user = new User();

        $input = Input::all();
        $input = nw
        $rules= array(
            'name'=>'required|unique:users|alpha_dash|min:4',
            'email'=>'required',
            'password'=>'required|alpha_num|confirmed',
            'password_confirmation'=>'required|alpha_num'
        );

        $v=Validator::make($input, $rules);
        if($v->fails()){
            return Redirect::to('user/new')->withErrors($v);
        }else{
            $user->name=$input['name'];
            $user->email=$input['email'];
            $user->password=Hash::make($input['password']);
            if($user->save()){
                return Redirect::to('user/login')->with(
                    array('SuccessFully Account Created! Please Login')
                );
            }else{
                return Redirect::to('user/new')->withInput();
            }
        }
    }

    public function getLogin(){
        return view('users.login')->with('title', 'Make it Snappy Q&A ->Login');
    }

    public function postLogin(){
        $loginInputs = Input::all();

        $loginRules = array(
            'name'=>'required|alpha_dash|min:4',
            'password'=>'required|alpha_num'
        );

        $v=Validator::make($loginInputs, $loginRules);
        if($v->fails()){
            return Redirect::to('user/login')->withErrors($v);
        }else{
            $crediantal = array(
                'name'=>$loginInputs['name'],
                'password'=>$loginInputs['password']
            );

            if(Auth::attempt($crediantal)){
                return Redirect::to('/home')->with(
                    array(
                        'Successfully loged in'
                    )
                );
            }else{
                return 'username or password failed';
            }
        }
    }

    public function getLogout(){
        if(Auth::user()){
            Auth::logout();
            return Redirect::to('user/login');
        }else{
            return Redirect::to('/home');
        }

    }


}
