<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>{{$title}}</title>
<link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}"/>
</head>
<body>
    <h1 class="text-center">Make it Snappy</h1>


   <div class="container">

        <nav class="nav nav-bar">
            <ul class="nav navbar-nav navbar-inverse">
                        <li><a href="{{url('/home')}}">Home</a></li>
                        @if(Auth::user())
                        <li><a href="{{url('user/logout')}}">Log out</a></li>
                        @else
                        <li><a href="{{url('user/new')}}">Register</a></li>
                        <li><a href="{{url('user/login')}}">Login</a></li>
                        @endif
            </ul>
        </nav>
        <div class="jumbotron">
            @if(Session::has('message'))
                <p>{{Session::get('message')}}</p>
            @endif

            @yield('content')

        </div>
        <div class="nav nav-bar">
            <p>copy make Question and <span style="color: red">{{date('Y')}}</span></p>
        </div>
   </div>
</body>
</html>