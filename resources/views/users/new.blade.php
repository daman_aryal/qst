@extends('layouts.default')
@section('content')
    <h1>This is Register Page</h1>

    <div class="row">
        <div class="col-md-4">
            @foreach($errors->all() as $error)
                <p style="color: red">{{$error}}</p>
            @endforeach
            <form action="{{url('user/new')}}" method="post">
            {{csrf_field()}}
                   <div class="form-group">
                    <input type="text" name="name" placeholder="Enter your username" class="form-control" value="{{old('name')}}"/>
                   </div>
                   <div class="form-group">
                     <input type="email" name="email" placeholder="Enter your email" class="form-control" value="{{old('email')}}"/>
                   </div>
                   <div class="form-group">
                   <input type="password" name="password" placeholder="Enter your password" class="form-control"/>
                   </div>
                   <div class="form-group">
                   <input type="password" name="password_confirmation" class="form-control" placeholder="Re-enter your password"/>
                   </div>
                    <input type="submit" value="Register" class="btn btn-default"/>
                </form>
        </div>
    </div>
@endsection