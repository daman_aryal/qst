@extends('layouts.default')
@section('content')
    @foreach($errors->all() as $error)
        <p style="color: red">{{$error}}</p>
    @endforeach
    <h1>Please Login</h1>
    <div class="row">
        <div class="col-md-4">
            <form action="{{url('user/login')}}" method="post">
            {{csrf_field()}}
                    <div class="form-group">
                        <input type="text" name="name" placeholder="Enter your Username" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" placeholder="Enter your password" class="form-control"/>
                    </div>
                    <input type="submit" value="Log in" class="btn btn-default"/>
                </form>
        </div>
    </div>
@endsection