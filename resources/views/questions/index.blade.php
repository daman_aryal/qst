@extends('layouts.default')
@section('content')
    @foreach($errors->all() as $error)
        <p>{{$error}}</p>
    @endforeach
    <span style="color: red">{{Session('msg')}}</span>
    @if(Auth::user())
    <form action="{{url('home/question')}}" method="post">
    {{csrf_field()}}
            <div class="form-group">
                <input type="text" name="question" class="form-control"/>
            </div>
            <input type="submit" value="Post Question" class="btn btn-default"/>
        </form>
    @else
   <h2> <a href="{{url('user/login')}}">Please Login First</a></h2>
    @endif
@endsection